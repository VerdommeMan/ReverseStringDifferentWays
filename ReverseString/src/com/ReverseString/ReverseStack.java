package com.ReverseString;

import java.util.Scanner;
import java.util.Stack;

public class ReverseStack {
	private  boolean game = true;
	
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		ReverseStack r = new ReverseStack();
		while(r.game) {
			
			String reverse = s.nextLine();
			if(reverse.equals("end")){
				r.game=false;
			}else{
				r.reverseStack(reverse);
			}
		}
		s.close();
	}
	public void reverseStack(String reverse) {
		Stack<String> stack =new Stack<String>();
		
		for(int i = 0; i<reverse.length();i++) {
			stack.push(reverse.substring(i,i+1));
		}
		for(int i = stack.size();i>0;i--) {
			System.out.print(stack.pop());
		}
		System.out.println();
	}
}
