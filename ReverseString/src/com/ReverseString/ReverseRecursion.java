package com.ReverseString;

import java.util.Scanner;

public class ReverseRecursion {
	private  boolean game = true;
	
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		ReverseRecursion r = new ReverseRecursion();
		while(r.game) {
			
			String reverse = s.nextLine();
			if(reverse.equals("end")){
				r.game=false;
			}else{
				r.reverse(reverse);
			}
		}
		s.close();
	}
	public void reverse(String yuppie) {
		if(yuppie.length()==0) {
			System.out.println();
		}else {
			System.out.print(yuppie.substring(yuppie.length()-1, yuppie.length()));
			reverse( yuppie.substring(0,yuppie.length()-1));
		}
		
	}
}
