package com.ReverseString;

import java.util.Scanner;

public class ReverseMethod {
	private  boolean game = true;
	
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		ReverseMethod r = new ReverseMethod();
		while(r.game) {
			
			String reverse = s.nextLine();
			if(reverse.equals("end")){
				r.game=false;
			}else{
				System.out.println(new StringBuilder(reverse).reverse().toString());
			}
		}
		s.close();
	}
}
