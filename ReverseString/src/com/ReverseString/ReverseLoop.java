package com.ReverseString;

import java.util.Scanner;

public class ReverseLoop {
	private  boolean game = true;
	
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		ReverseLoop r = new ReverseLoop();
		while(r.game) {
			
			String reverse = s.nextLine();
			if(reverse.equals("end")){
				r.game=false;
			}else{
				for(int i =reverse.length();i>0;i--) {
					System.out.print(reverse.substring(i-1, i));
				}
				System.out.println();
			}
		}
		s.close();
	}
}
